<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <style type="text/css">
        #lay_show img {
            max-width: 100% !important;
            width: 100% !important;
            height: auto !important;
            object-fit: cover !important;
            object-position: center !important;
        }
    </style>
</head>
<body>
    <input type="hidden" id="base_url" value="<?php echo base_url();?>">
    <div class="container">
        <form action="<?php echo site_url('upload/add');?>" method="POST" id="form" class="mt-5">
            <div class="form-group">
                <textarea name="my_textarea" id="my_textarea"></textarea>
            </div>
            <button id="btn_submit" type="submit" class="btn btn-primary">Send</button>
        </form>

        <div id="lay_show" class="mt-5">
            <?php
            $this->db->order_by('id','DESC');
            $query = $this->db->get('textarea')->result();
            
            if (count($query) > 0) {
                foreach ($query as $value) {
                    echo '
                    <div class="border my-5 px-2 py-2">
                        '.$value->description.'
                    </div>';
                }
            }
            ?>
        </div>
    </div>



    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/tinymce/tinymce.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/script.js');?>"></script>
</body>
</html>