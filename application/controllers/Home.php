<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('tinymce');
	}

	public function upload_img()
	{
		//Allowed origins to upload images
		$accepted_origins = array("http://localhost", "http://107.161.82.130", "http://codexworld.com");

		// Images upload path
		$imageFolder = "images/";

		reset($_FILES);
		$temp = current($_FILES);
		if(is_uploaded_file($temp['tmp_name'])){
		    if(isset($_SERVER['HTTP_ORIGIN'])){
		        // Same-origin requests won't set an origin. If the origin is set, it must be valid.
		        if(in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)){
		            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
		        }else{
		            header("HTTP/1.1 403 Origin Denied");
		            return;
		        }
		    }
		  
		    // Sanitize input
		    if(preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])){
		        header("HTTP/1.1 400 Invalid file name.");
		        return;
		    }
		  
		    // Verify extension
		    if(!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))){
		        header("HTTP/1.1 400 Invalid extension.");
		        return;
		    }
		  
		    // Accept upload if there was no origin, or if it is an accepted origin
		    $filetowrite = $imageFolder . $temp['name'];
		    move_uploaded_file($temp['tmp_name'], $filetowrite);
		  
		    // Respond to the successful upload with JSON.
		    echo json_encode(array('location' => $filetowrite));
		} else {
		    // Notify editor that the upload failed
		    header("HTTP/1.1 500 Server Error");
		}
	}

	public function add_data()
	{
		$result['data'] = "false";

		$text = $this->input->post('my_textarea');

		$data = array('description' => $text);

		$insert = $this->db->insert('textarea', $data);
		if ($insert) {
			$result['data'] = "success";
		}

		echo json_encode($result);
	}

	public function show()
	{
		$this->db->order_by('id','DESC');
		$query = $this->db->get('textarea')->result();
		
		if (count($query) > 0) {
			foreach ($query as $value) {
				echo '
				<div class="border my-5 px-2 py-2">
					'.$value->description.'
				</div>';
			}
		}
	}
}
