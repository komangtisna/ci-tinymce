$(document).ready(function() {
  var base_url = $("#base_url").val();

  tinymce.init({
    selector: '#my_textarea',
    height: 500,
    theme: 'modern',
    plugins: [
      'advlist autolink lists link image charmap print preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code fullscreen',
      'insertdatetime media nonbreaking save table contextmenu directionality',
      'emoticons template paste textcolor colorpicker textpattern imagetools'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    toolbar2: 'print preview media | forecolor backcolor emoticons',
    image_advtab: true,
    relative_urls : false,
    remove_script_host : false,
    // without images_upload_url set, Upload tab won't show up
    images_upload_url: base_url+'upload',
    
    // override default upload handler to simulate successful upload
    images_upload_handler: function (blobInfo, success, failure) {
      var xhr, formData;
    
      xhr = new XMLHttpRequest();
      xhr.withCredentials = false;
      xhr.open('POST', base_url+'upload');
    
      xhr.onload = function() {
          var json;
      
          if (xhr.status != 200) {
              failure('HTTP Error: ' + xhr.status);
              return;
          }
      
          json = JSON.parse(xhr.responseText);
      
          if (!json || typeof json.location != 'string') {
              failure('Invalid JSON: ' + xhr.responseText);
              return;
          }
      
          success(json.location);
      };
    
      formData = new FormData();
      formData.append('file', blobInfo.blob(), blobInfo.filename());
    
      xhr.send(formData);
    },
  });

  get_data();
  function get_data() {
    $.ajax({
      type: "POST",
      url: base_url+'upload/show',
      cache: false,
      success: function(response){
        $('#lay_show').html(response);
      }
    });
  }


  $('#btn_submit').click(function() {
    $('#form').ajaxForm({
      beforeSend: function(){
        $('#btn_submit').attr('disabled','disabled');
      },
      complete: function(response, textStatus, XMLHttpRequest) {
        var responseText = jQuery.parseJSON(response.responseText);
        if(responseText.data=="success"){
          alert("success");
          $('#btn_submit').removeAttr('disabled');
          get_data();
        } else {
          alert("error");
          $('#btn_submit').removeAttr('disabled');
        }
      },
      error: function(){
        $('#btn_submit').removeAttr('disabled');
        alert('Your browser not support javascript');
      }
    });
  })
});